package com.viemall.demo.controller;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.viemall.demo.service.OrderService;

/**
 * 测试
 * @author Tony
 */
@Controller
public class TestController {
	
	private Logger logger=LoggerFactory.getLogger(TestController.class);
	
	@Autowired
	private OrderService orderService;

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public  String test(HttpServletRequest request,Model model) {
		logger.info("sesssion ID=>>>>>>>>>>>>>>>>>>>>>>>>> {} ",request.getSession().getId());
		System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		String result= orderService.orderPay(new BigDecimal(33.5));
		System.out.println("支付回调===>"+result);
		request.setAttribute("sesssionId", request.getSession().getId());
		return "success"; // 页面的名称，根据此字符串会去寻找名为HelloWorld.jsp的页面
	}
	

}