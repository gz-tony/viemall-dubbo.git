package com.viemall.config;

import org.springframework.stereotype.Service;

import com.baidu.disconf.client.common.annotations.DisconfFile;
import com.baidu.disconf.client.common.annotations.DisconfFileItem;
import com.baidu.disconf.client.common.annotations.DisconfUpdateService;
import com.baidu.disconf.client.common.update.IDisconfUpdate;

/**
 * application配置文件
 */
@Service
@DisconfFile(filename = "application.properties")
@DisconfUpdateService(classes = {ApplicationConfig.class})
public class ApplicationConfig implements IDisconfUpdate {

    private String dubboAddress;
    
    private String legendshop_version;
    
    private String sql_debug_mode;
    
    private String currency_pattern;
    
    private String authentication_mode;
    
    private String business_mode;
    
    private String language_mode;
    
    private String images_path_prefix;
    
    private String photo_path_prefix;
    
    private String snap_photo_path_prefix;
    
    
    @DisconfFileItem(name = "dubbo.registry.address", associateField = "dubboAddress")
	public String getDubboAddress() {
		return dubboAddress;
	}

	public void setDubboAddress(String dubboAddress) {
		this.dubboAddress = dubboAddress;
	}
	
	/**
	 * #LegendShop版本号
	 * @return
	 */
	 @DisconfFileItem(name = "LEGENDSHOP_VERSION", associateField = "legendshop_version")
	public String getLegendshop_version() {
		return legendshop_version;
	}

	public void setLegendshop_version(String legendshop_version) {
		this.legendshop_version = legendshop_version;
	}

	/**
	 * #DAL.cfg.xml是否采用debug模式，如果是则在开发环境中每次更改SQL等配置都会立即生效，生产环境建议采用false
	 * @return
	 */
	 @DisconfFileItem(name = "SQL_DEBUG_MODE", associateField = "sql_debug_mode")
	public String getSql_debug_mode() {
		return sql_debug_mode;
	}

	public void setSql_debug_mode(String sql_debug_mode) {
		this.sql_debug_mode = sql_debug_mode;
	}

	/**
	 * #货币格式
	 * @return
	 */
	 @DisconfFileItem(name = "CURRENCY_PATTERN", associateField = "currency_pattern")
	public String getCurrency_pattern() {
		return currency_pattern;
	}

	public void setCurrency_pattern(String currency_pattern) {
		this.currency_pattern = currency_pattern;
	}

	/**
	 * #验证登录模式，可选值
	#basic 采用普通security方式
	#cas  采用cas单点登录模式，需要配合cas系统使用
	 * @return
	 */
	 @DisconfFileItem(name = "AUTHENTICATION_MODE", associateField = "authentication_mode")
	public String getAuthentication_mode() {
		return authentication_mode;
	}

	public void setAuthentication_mode(String authentication_mode) {
		this.authentication_mode = authentication_mode;
	}

	 @DisconfFileItem(name = "BUSINESS_MODE", associateField = "business_mode")
	public String getBusiness_mode() {
		return business_mode;
	}

	public void setBusiness_mode(String business_mode) {
		this.business_mode = business_mode;
	}

	/**
	 * #系统语言模式，可选项为english 英文， chinese 中文， userChoice 跟浏览器配置
	 * @return
	 */
	 @DisconfFileItem(name = "LANGUAGE_MODE", associateField = "language_mode")
	public String getLanguage_mode() {
		return language_mode;
	}

	public void setLanguage_mode(String language_mode) {
		this.language_mode = language_mode;
	}

	
	/**
	 * #缩略图前缀，采用ImagesServlet来提供服务，后面必须带上"/"
	 * @return
	 */
	 @DisconfFileItem(name = "IMAGES_PATH_PREFIX", associateField = "images_path_prefix")
	public String getImages_path_prefix() {
		return images_path_prefix;
	}

	public void setImages_path_prefix(String images_path_prefix) {
		this.images_path_prefix = images_path_prefix;
	}

	/**
	 * #大图前缀，采用PhotoServlet来提供服务，后面必须带上"/"
	 * @return
	 */
	 @DisconfFileItem(name = "PHOTO_PATH_PREFIX", associateField = "photo_path_prefix")
	public String getPhoto_path_prefix() {
		return photo_path_prefix;
	}

	public void setPhoto_path_prefix(String photo_path_prefix) {
		this.photo_path_prefix = photo_path_prefix;
	}

	/**
	 * #商品快照前缀，后面必须带上"/"
	 * @return
	 */
	 @DisconfFileItem(name = "SNAP_PHOTO_PATH_PREFIX", associateField = "snap_photo_path_prefix")
	public String getSnap_photo_path_prefix() {
		return snap_photo_path_prefix;
	}

	public void setSnap_photo_path_prefix(String snap_photo_path_prefix) {
		this.snap_photo_path_prefix = snap_photo_path_prefix;
	}

	@Override
	public void reload() throws Exception {
		 System.out.println("ApplicationConfig >>>>> 数据更新..... ");
		 System.out.println(this.toString());
	}

	@Override
	public String toString() {
		return "ApplicationConfig [dubboAddress=" + dubboAddress + ", legendshop_version=" + legendshop_version
				+ ", sql_debug_mode=" + sql_debug_mode + ", currency_pattern=" + currency_pattern
				+ ", authentication_mode=" + authentication_mode + ", business_mode=" + business_mode
				+ ", language_mode=" + language_mode + ", images_path_prefix=" + images_path_prefix
				+ ", photo_path_prefix=" + photo_path_prefix + ", snap_photo_path_prefix=" + snap_photo_path_prefix
				+ "]";
	}


    
}
