package com.viemall.demo.service.impl;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.viemall.demo.service.OrderService;

/**
 * 
 * @author Tony
 *
 */
public class OrderServiceImpl implements OrderService {

	private Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

	public String orderPay(BigDecimal money) {

		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$ DUBBO Service 调用了 $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

		return "Paid Successfully " + money;
	}

}
