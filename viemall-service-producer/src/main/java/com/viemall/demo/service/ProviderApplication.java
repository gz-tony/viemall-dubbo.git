package com.viemall.demo.service;

import java.util.concurrent.CountDownLatch;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 
 * @author Tony
 *
 */
public class ProviderApplication {

	private static CountDownLatch countDownLatch = new CountDownLatch(1);

	/**
	 * 这里可以采用Spring boot 如果没有web容器，需要hold住服务,否则进程会退出,参考以下代码:
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			ApplicationContext context = new ClassPathXmlApplicationContext(
					new String[] { "classpath:applicationContext-service.xml", "classpath:spring-dubbo-provider.xml",
							"classpath:spring-dubbo-consumer.xml" });
			System.out.println("-------------------------->启动成功-------------------------->");
			countDownLatch.await();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
