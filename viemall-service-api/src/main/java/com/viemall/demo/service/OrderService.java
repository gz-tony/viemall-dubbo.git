package com.viemall.demo.service;

import java.math.BigDecimal;

/**
 * 
 * @author Tony
 *
 */
public interface OrderService {

	/**
	 * 订单支付服务
	 * @param money
	 * @return
	 */
	String orderPay(BigDecimal money);
}
